#ifndef SEJTABLAK_H
#define SEJTABLAK_H

#include <QMainWindow>
#include <QPainter>
#include "sejtszal.h"

class SejtSzal;

class SejtAblak : public QMainWindow
{
  Q_OBJECT

public:
  SejtAblak(int szelesseg = 100, int magassag = 75, QWidget *parent = 0);

  ~SejtAblak();
  // Egy sejt lehet \E9l\F5
  static const bool ELO = true;
  // vagy halott
  static const bool HALOTT = false;
  void vissza(int racsIndex);

protected:
  // K\E9t r\E1csot haszn\E1lunk majd, az egyik a sejtt\E9r \E1llapot\E1t
  // a t_n, a m\E1sik a t_n+1 id\F5pillanatban jellemzi.
  bool ***racsok;
  // Valamelyik r\E1csra mutat, technikai jelleg\FB, hogy ne kelljen a
  // [2][][]-b\F3l az els\F5 dimenzi\F3t haszn\E1lni, mert vagy az egyikre
  // \E1ll\EDtjuk, vagy a m\E1sikra.
  bool **racs;
  // Megmutatja melyik r\E1cs az aktu\E1lis: [r\E1csIndex][][]
  int racsIndex;
  // Pixelben egy cella adatai.
  int cellaSzelesseg;
  int cellaMagassag;
  // A sejtt\E9r nagys\E1ga, azaz h\E1nyszor h\E1ny cella van?
  int szelesseg;
  int magassag;    
  void paintEvent(QPaintEvent*);
  void siklo(bool **racs, int x, int y);
  void sikloKilovo(bool **racs, int x, int y);

private:
  SejtSzal* eletjatek;

};

#endif // SEJTABLAK_H
