<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Liskov!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>



    <section>    
       <title>Liskov helyettesítés sértése</title>
       

<para><emphasis>Írjunk olyan OO, leforduló Java és C++ kódcsipetet, amely megsérti a Liskov
                elvet! Mutassunk rá a megoldásra: jobb OO tervezés.</emphasis></para>
        <para><emphasis>Számos példa szerepel az elv megsértésére az UDPROG repóban, lásd pl.
                source/binom/Batfai-Barki/madarak/</emphasis></para>
        <para> Megoldás forrása: <link
                xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_1.pdf"
            /> (93-99 fólia)</para>
        <para>Egy amerikai szoftverfejlesztő nevéhez, Robert Cecil Martin-hoz tudjuk fűzni a
            S.O.L.I.D. fogalmat. Valószínűleg kezdő programozóként nem nagyon találkozik vele az
            ember, de ahogy lassan előrébb jut az ember és jobban belemélyül az objektumorientált
            programozásba, bizony ezzel is fog találkozni, ahogy mi most. Minden betű egy-egy
            szabály nevét rövidíti le, amikről nagyon sokat lehetne mesélni, de mi most a harmadik
            betűvel ismerkednénk meg először: L, mint LSP (Liskov's Substition Principle). Barbara
            Liskov egy ma is élő informatikus, aki először szerzett doktori fokozatot az USA-ban,
            mint nő, illetve Turing-díjat is nyert. Ő fejlesztette ki a Liskov helyettesítés elvét,
            ami azt mondja, hogy a programban nem okoz problémát vagy nem eredményez rossz
            megoldást, hogyha bárhol, ahol az ősosztály példányát használnánk, helyette az
            alosztályt alkalmaznánk. Íme a tétel, magyarra lefordítva:</para>
	<para><emphasis>Ha S altípusa T-nek, akkor minden olyan helyen ahol T-t felhasználjuk
                    S-t is minden gond nélkül behelyettesíthetjük anélkül, hogy a programrész
                    tulajdonságai megváltoznának.</emphasis></para>
	<para>(<link xlink:href="https://reiteristvan.wordpress.com/2011/07/05/s-o-l-i-d-objektum-orientalt-tervezesi-elvek-3-lsp/">
	Reiter István</link> fordítása)</para>
        <para>Az elv viszont nem mindenesetben állja meg a helyét. Erre a két legismertebb példa a
            téglalap-négyzet és a kör-ellipszis. Az előbbit nézve például, a téglalap egy olyan
            alakzat, aminek két szembe lévő oldala megegyezik. És ebbe beletartozik a négyzet is,
            hiszen két szemközti oldala egyenlő. Viszont ha úgy nézzük, hogy a négyzet lenne az
            ősosztály, az ő feltételei azt mondanák, hogy minden oldala egyenlő, emiatt nem lehet a
            téglalap az alosztálya, hiszen annál ez az állítás nem igaz. Kicsit olyan, hogy minden
            bogár rovar, de nem minden rovar bogár. És maradjunk az állatoknál, velük nézzünk meg
            egy-egy példát Java-ra és C++-ra is, mert velük talán kicsit barátságosabb ez a téma. </para>
        <para>Az első C++ példában madarakról lesz szó, amit Bátfai Norbert tanárúr példa kódja, ezt
            fogjuk kicsit szétszedni, és érthetőbben elmagyarázni, miért is sérti meg ez a Liskov
            elvet. Egyébként maga a program nem bonyolult, hiszen csak osztályok vannak benne, mivel
            nincs egy konkrét célunk, amit végre kell hajtania a programnak. </para>
        <para>
            <programlisting>class Madar {
public:
     virtual void repul() {};
};

class Program {
public:
     void fgv ( Madar &amp;madar ) {
          madar.repul();
     }
};

class Sas : public Madar
{};

class Pingvin : public Madar
{};

int main ( int argc, char **argv )
{
     Program program;
     Madar madar;
     program.fgv ( madar );

     Sas sas;
     program.fgv ( sas );

     Pingvin pingvin;
     program.fgv ( pingvin ); 

}</programlisting>
        </para>
        <para>Két fő osztályunk van, a <classname>Program</classname> és a
                <classname>Madar</classname>, mind a kettő nyilvános. Az utóbbiban egy
                <function>repul()</function> függvényt "írtunk meg" (szóban megegyezhetünk, hogy
            repültetné a madarat), az előbbi pedig ezt a függvényt felhasználva csinált egy
            eljárást, így a main-ben majd nyilván ezt fogjuk használni, ő végzi a programot az
            osztályokkal. Utánuk jön két alosztály a <classname>Madar</classname> számára, az egyik
            a <classname>Sas</classname>, a másik a <classname>Pingvin</classname>. Mind a kettő
            madár, bizonyos tulajdonságokban megegyeznek. De azért mégis akad egy nagy különbség
            közöttük: a pingvin nem tud repülni. Így a main részben a
                <function>program.fgv()</function> függvény nyilván le tud futni a
                <classname>Madar</classname> és a <classname>Sas</classname> osztályokkal, de a
                <classname>Pingvin</classname> esetében nem az előbb említett ok miatt. Persze a
            program lefut így is, de ha tényleg olyan beállításokat tennénk be, amik miatt a
                <classname>Pingvin</classname> utófeltételei gyengébbek lennének, mint a
                <classname>Madar</classname> utófeltételei, akkor már nem működne az alkalmazás.
            Elég logikus, nem? </para>
        <para>Nézzük a Java példát, már kutyával és macskával nézve. Mind a kettőnek van neve,
            fajtája, ugyanannyi lába és a többi, de például a hangutánzásuk különböző, és pont
            emiatt tudjuk megsérteni majd a Liskov elvet. </para>
        <para>
            <programlisting>public class Liskov {

class Allatok {
    public void ugat() {
        System.out.println("Ugatok.");
    }
}

class Program {
    public void fgv(Allatok allatok) {
        allatok.ugat();
    }
}

class Kutya extends Allatok {
}

class Macska extends Allatok {
}

public static void main(String[] args) {
    Liskov lis = new Liskov();

    Program program = lis.new Program();
    Allatok allatok = lis.new Allatok();
    program.fgv(allatok);
	
    Kutya kutya = lis.new Kutya();
    program.fgv(kutya);

    Macska macska = lis.new Macska();
    program.fgv(macska);
    }
}</programlisting>
        </para>
        <para>Létrehoztunk egy <classname>Allatok</classname> osztályt, amiben meghatároztunk egy
                <function>ugat()</function> függvényt. Ez persze csak a kutyákra lesz igaz, direkt
            csak rájuk fókuszálunk - így látjuk azt is majd, hogy ha egyes gyerekekre vonatkozik egy
            függvény vagy tulajdonság, azt nekik adjuk, ne a szülőnek -, de mondjuk írhattuk volna
            azt is, hogy négy lábú állatok, és abban az esetben már például egy pingvin kiesett
            volna a számításból. De most maradjunk ennél a kutya-macska párosításnál. Itt is
            létrehoztuk a <classname>Program</classname> osztályunkat is, ami a feladatot végzi majd
            el. Végül létrehoztuk a két gyerek osztályt: <classname>Kutya</classname> és
                <classname>Macska</classname>. Elég egyértelmű, hogy a <function>main()</function>
            függvényben az előbbi le fog futni, az utóbbi nem (illetve most igen, de mi tudjuk, hogy
            lehetetlen, hogy egy macska ugasson). Így ez bizonyítja azt, hogy a szülő elemben lévő
            tulajdonságok, függvények nem feltétlenül igazak annak gyerekeire is, azaz a Liskov
            elvet megsértettük a programunkkal. </para>

     
 
    </section>        
 















<section>
        <title>Szülő-gyerek</title>
        

        <para><emphasis>Írjunk Szülő-gyerek Java és C++ osztálydefiníciót, amelyben demonstrálni tudjuk, hogy az ősön keresztül csak az ős üzenetei küldhetőek!</emphasis></para>
        <para> Megoldás forrása: <link
                xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_1.pdf"
            /> (98. fólia)</para>
        <para>Egy újabb feladat az öröklődéssel kapcsolatban. Az előző feladatban pontosan arról
            volt szó, hogy a gyerek amúgy eléri a szülő public metódusait, de vannak bizonyos
            esetek, mikor mégsem. Most nézzük meg a másik oldalát, azaz mikor egy szülő akarja
            elérni a gyereknek egy függvényét például. Az öröklődés szabálya úgy hangzik, hogy egy
            gyerek eléri a szülőjének az elemeit, amik nyilvánosra vannak állítva, de ha már a
            gyereken belül írunk meg például egy függvényt, ahhoz a szülő már nem fér hozzá, csakis
            a gyerek és annak esetleges alosztályai. A feladatunk az, hogy ezt bebizonyítsuk először
            Java, majd C++ nyelven. </para>
        <para>
            <programlisting>public class Szulo_gyerek{
	class Szulo {
		
	}

	class Gyerek extends Szulo {
		public void viselkedes() {
			System.out.println("Viselkedek vagy nem viselkedek?");
		}
	}
	
	public static void main(String[] args) {
			Szulo_gyerek sz_gy = new Szulo_gyerek();
				
			Szulo szulo = sz_gy.new Gyerek();
			Gyerek gyerek = sz_gy.new Gyerek();

			gyerek.viselkedes();    //le fog futni
			szulo.viselkedes();    //hibát ad vissza
	
		}
}</programlisting>
        </para>
        <para>Létrehoztunk egy <classname>Szulo</classname> osztályt, ahogy a neve is mondja, ő lesz
            a főosztályunk, és neki lesznek alosztályai, illetve most csak egy, a
                <classname>Gyerek</classname>. Az utódosztályban írunk egy függvényt, ami csak kiír
            egy kérdést, így látjuk majd, hogy mely esetekben fut majd le. Az előző kód esetében nem
            tértem ki rá, de a kódban az <code>extends</code> utal arra, hogy azaz osztály
            valamelyiknek az alosztálya. Elé írjuk az alosztály nevét, utána pedig az ősosztályét.
            Ez nagyon fontos, mert így jelezzük a hierarchiát a programunknak. </para>
        <para>A <function>main()</function> függvényünkben példányosítottuk az <code>sz_gy</code>
            metódust, ennek a segítségével határozzuk meg a main-en belül a sorrendet, azaz hogy a
            szülő és a gyerek is ezen belül található. Ha ezt kivennénk, akkor nem működne a
            programunk. Ennek segítségével hoztuk létre a <code>szulo</code> és <code>gyerek</code>
            példányokat, akikkel lefuttatjuk a <function>viselkedes()</function> függvényt. Illetve
            csak próbáljuk. Ha csak a gyerekkel próbálkozunk, a függvény lefut és kiírja a
            szövegünket, viszont ha hozzáírjuk a szülőt is, a program hibát fog visszadobni, hiszen
            a szülő nem éri el a gyerek saját metódusait. </para>
        <para>
            <programlisting>class Szulo {
};

class Gyerek : public Szulo {
public:
    void viselkedes() {
        cout &lt;&lt; "Viselkedek vagy nem viselkedek?";
    }
};

int main ( int argc, char **argv )
{
     viselkedes(szulo);
     viselkedes(gyerek);
}</programlisting>
        </para>
        <para>Az előző Java-s feladat C++-os implementációja, így igazán nincs különbség a kettő
            között, inkább csak szintaxisbeli változásokat tapasztalhatunk. Természetesen itt is
            csak a gyerek esetében írná ki a szövegünket, a szülő esetében már nem, a feljebb is
            megírt ok miatt. </para>


       

        
    </section>      




















        
    








    <section>
        <title>Anti OO</title>
        <para><emphasis>A BBP algoritmussal a Pi hexadecimális kifejtésének a 0. pozíciótól számított 10<superscript>6</superscript>, 10<superscript>7</superscript>, 10<superscript>8</superscript> darab jegyét határozzuk meg C, C++, Java és C# nyelveken és vessük össze a futási időket! </emphasis></para>
        <para> Megoldás forrása: <link
                xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apas03.html#id561066"
            /> és <link
                xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apbs02.html#pi_jegyei"
            /></para>
        <para>Harmadik feladatunk nem idegen számunkra, hiszen az előző feladatsorban, az
            Arroway-ben már találkoztunk ezzel, ahol részleteztük a C++ és a Java változatokat. Most
            még hozzáadjuk a C és a C# verziókat is, amik csak szintaxisban különböznek igazából,
            így kifejteni őket teljesen felesleges, nem is ezt kéri tőlünk a feladat, hanem az
            összehasonlításukat futási idő szempontjából. Ezek az eredmények természetesen sok
            mindentől függnek, mint például milyen processzor alatt végezzük a mérést, optimalizált
            környezetben történtek-e a futások, milyen verziószámon és a többi. </para>
        <para>Azonban még pár dolgot gyorsan tisztáznunk kell, mielőtt neki ugranánk a futtatásnak
            és azt hinnénk, hogy elvégeztük a feladatot. Azon kívül, hogy a for ciklusban mindig
            bele kell piszkálnunk attól függően, hogy a 10-et éppen hanyadikra emeljük, a Java
            változatunkból ki kell vennünk az objektumorientáltságot annak érdekében, hogy
            egységesebbek legyenek a kódok, vagyis próbáljuk hasonló esélyekkel indítani őket. </para>
	




<para>A program C
            nyelven:<programlisting>#include &lt;stdio.h>
#include &lt;math.h>
#include &lt;time.h>
/*
 * pi_bbp_bench.c
 *
 * DIGIT 2005, Javat tanítok
 * Bátfai Norbert, nbatfai@inf.unideb.hu
 *
 * A PiBBP.java-ból kivettük az "objektumorientáltságot", így kaptuk
 * a PiBBPBench osztályt, amit pedig átírtuk C nyelvre.
 *
 */

/*
 * 16^n mod k
 * [BBP ALGORITMUS] David H. Bailey: The
 * BBP Algorithm for Pi. alapján.
 */
long
n16modk (int n, int k)
{
  long r = 1;

  int t = 1;
  while (t &lt;= n)
    t *= 2;

  for (;;)
    {

      if (n >= t)
	{
	  r = (16 * r) % k;
	  n = n - t;
	}

      t = t / 2;

      if (t &lt; 1)
	break;

      r = (r * r) % k;

    }

  return r;
}

/* {16^d Sj}
 * [BBP ALGORITMUS] David H. Bailey: The
 * BBP Algorithm for Pi. alapján.
 */
double
d16Sj (int d, int j)
{

  double d16Sj = 0.0;
  int k;

  for (k = 0; k &lt;= d; ++k)
    d16Sj += (double) n16modk (d - k, 8 * k + j) / (double) (8 * k + j);

  /*
     for(k=d+1; k&lt;=2*d; ++k)
     d16Sj += pow(16.0, d-k) / (double)(8*k + j);
   */

  return d16Sj - floor (d16Sj);
}

/*
 * {16^d Pi} = {4*{16^d S1} - 2*{16^d S4} - {16^d S5} - {16^d S6}}
 * [BBP ALGORITMUS] David H. Bailey: The
 * BBP Algorithm for Pi. alapján.
 */
main ()
{

  double d16Pi = 0.0;

  double d16S1t = 0.0;
  double d16S4t = 0.0;
  double d16S5t = 0.0;
  double d16S6t = 0.0;

  int jegy;
  int d;

  clock_t delta = clock ();</programlisting></para>
        <para>A programban az alábbi helyen szükséges módosítanunk, hogy a különböző hatványokon
            kapott számjegyeket visszakapjuk.</para>
        <programlisting>for (d = 1000000; d &lt; 1000001; ++d)</programlisting>
        <para>Ez a prefix a 10^6. számjegyet adja vissza, amennyiben ezen változtatni szeretnénk, a
            0-k számát növeljük 1-gyel, így visszakapjuk a 10^7. számjegyet és így tovább. Mind a 4
            nyelven megírt porgramban ugyanitt és ugyanígy kell végrehajtanunk a módosítást, így ezt
            külön többször nem emelném ki.</para>
        <programlisting>{

      d16Pi = 0.0;

      d16S1t = d16Sj (d, 1);
      d16S4t = d16Sj (d, 4);
      d16S5t = d16Sj (d, 5);
      d16S6t = d16Sj (d, 6);

      d16Pi = 4.0 * d16S1t - 2.0 * d16S4t - d16S5t - d16S6t;

      d16Pi = d16Pi - floor (d16Pi);

      jegy = (int) floor (16.0 * d16Pi);

    }

  printf ("%d\n", jegy);
  delta = clock () - delta;
  printf ("%f\n", (double) delta / CLOCKS_PER_SEC);
} </programlisting>
        <para>Egy kép a futásról:</para>
        <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/PiBPP_c.png"/>
                </imageobject>
            </inlinemediaobject></para>
        <para>Megoldás
            C++-ban:<programlisting>#include &lt;stdio.h>
#include &lt;math.h>
#include &lt;time.h>
#include &lt;iostream>
/*
 * pi_bbp_bench.c
 *
 * DIGIT 2005, Javat tanítok
 * Bátfai Norbert, nbatfai@inf.unideb.hu
 *
 * A PiBBP.java-ból kivettük az "objektumorientáltságot", így kaptuk
 * a PiBBPBench osztályt, amit pedig átírtuk C nyelvre.
 *
 */

/*
 * 16^n mod k
 * [BBP ALGORITMUS] David H. Bailey: The
 * BBP Algorithm for Pi. alapján.
 */
long
n16modk (int n, int k)
{
  long r = 1;

  int t = 1;
  while (t &lt;= n)
    t *= 2;

  for (;;)
    {

      if (n >= t)
	{
	  r = (16 * r) % k;
	  n = n - t;
	}

      t = t / 2;

      if (t &lt; 1)
	break;

      r = (r * r) % k;

    }

  return r;
}

/* {16^d Sj}
 * [BBP ALGORITMUS] David H. Bailey: The
 * BBP Algorithm for Pi. alapján.
 */
double
d16Sj (int d, int j)
{

  double d16Sj = 0.0;
  int k;

  for (k = 0; k &lt;= d; ++k)
    d16Sj += (double) n16modk (d - k, 8 * k + j) / (double) (8 * k + j);

  /*
     for(k=d+1; k&lt;=2*d; ++k)
     d16Sj += pow(16.0, d-k) / (double)(8*k + j);
   */

  return d16Sj - floor (d16Sj);
}

/*
 * {16^d Pi} = {4*{16^d S1} - 2*{16^d S4} - {16^d S5} - {16^d S6}}
 * [BBP ALGORITMUS] David H. Bailey: The
 * BBP Algorithm for Pi. alapján.
 */
main ()
{

  double d16Pi = 0.0;

  double d16S1t = 0.0;
  double d16S4t = 0.0;
  double d16S5t = 0.0;
  double d16S6t = 0.0;

  int jegy;
  int d;

  clock_t delta = clock ();

  for (d = 1000000; d &lt; 1000001; ++d)
    {

      d16Pi = 0.0;

      d16S1t = d16Sj (d, 1);
      d16S4t = d16Sj (d, 4);
      d16S5t = d16Sj (d, 5);
      d16S6t = d16Sj (d, 6);

      d16Pi = 4.0 * d16S1t - 2.0 * d16S4t - d16S5t - d16S6t;

      d16Pi = d16Pi - floor (d16Pi);

      jegy = (int) floor (16.0 * d16Pi);

    }

  std::cout&lt;&lt;jegy&lt;&lt;std::endl;
  delta = clock () - delta;
  std::cout&lt;&lt;((double) delta / CLOCKS_PER_SEC)&lt;&lt;std::endl;
} </programlisting></para>
        <para>Futásról készült kép:</para>
        <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/PiBPP_cpp.png"/>
                </imageobject>
            </inlinemediaobject></para>
        <para>Megoldás
            Java-ban:<programlisting>public class PiBBPBench {
   
    public static double d16Sj(int d, int j) {
        
        double d16Sj = 0.0d;
        
        for(int k=0; k&lt;=d; ++k)
            d16Sj += (double)n16modk(d-k, 8*k + j) / (double)(8*k + j);
        
        
        return d16Sj - Math.floor(d16Sj);
    }
   
    public static long n16modk(int n, int k) {
        
        int t = 1;
        while(t &lt;= n)
            t *= 2;
        
        long r = 1;
        
        while(true) {
            
            if(n >= t) {
                r = (16*r) % k;
                n = n - t;
            }
            
            t = t/2;
            
            if(t &lt; 1)
                break;
            
            r = (r*r) % k;
            
        }
        
        return r;
    }
    
    public static void main(String args[]) {
        
        double d16Pi = 0.0d;
        
        double d16S1t = 0.0d;
        double d16S4t = 0.0d;
        double d16S5t = 0.0d;
        double d16S6t = 0.0d;
        
        int jegy = 0;
        
        long delta = System.currentTimeMillis();
        
        for(int d=1000000; d&lt;1000001; ++d) {
            
            d16Pi = 0.0d;
            
            d16S1t = d16Sj(d, 1);
            d16S4t = d16Sj(d, 4);
            d16S5t = d16Sj(d, 5);
            d16S6t = d16Sj(d, 6);
            
            d16Pi = 4.0d*d16S1t - 2.0d*d16S4t - d16S5t - d16S6t;
            
            d16Pi = d16Pi - Math.floor(d16Pi);
            
            jegy = (int)Math.floor(16.0d*d16Pi);
            
        }
        
        System.out.println(jegy);
        delta = System.currentTimeMillis() - delta;
        System.out.println(delta/1000.0);
    }
}</programlisting></para>
        <para>A futásról készült kép:</para>
        <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/PiBPP_java.png"/>
                </imageobject>
            </inlinemediaobject></para>
        <para>Megoldás
            C#-ban:<programlisting>/*
 * FileName: PiBBPBench.cs
 * Author: Bátfai Norbert, nbatfai@inf.unideb.hu
 * DIGIT 2005, Javat tanítok
 */
/// &lt;summary>
/// A PiBBPBench C# átírata.
/// &lt;/summary>
/// &lt;remark>
/// A PiBBP.java-ból kivettük az "objektumorientáltságot", így kaptuk
/// a PiBBPBench osztályt, amit pedig átírtuk C# nyelvre.
///
/// (A PiBBP osztály a BBP (Bailey-Borwein-Plouffe) algoritmust a Pi hexa
/// jegyeinek számolását végző osztály. A könnyebb olvahatóság
/// kedvéért a változó és metódus neveket megpróbáltuk az algoritmust
/// bemutató [BBP ALGORITMUS] David H. Bailey: The BBP Algorithm for Pi.
/// cikk jelöléseihez.)
/// &lt;/remark>
public class PiBBPBench {
    /// &lt;remark>
    /// BBP algoritmus a Pi-hez, a [BBP ALGORITMUS] David H. Bailey: The
    /// BBP Algorithm for Pi. alapján a {16^d Sj} részlet kiszámítása.
    /// &lt;/remark>
    /// &lt;param>
    /// d   a d+1. hexa jegytől számoljuk a hexa jegyeket
    /// &lt;/param>
    /// &lt;param>
    /// j   Sj indexe
    /// &lt;/param>
    public static double d16Sj(int d, int j) {
        
        double d16Sj = 0.0d;
        
        for(int k=0; k&lt;=d; ++k)
            d16Sj += (double)n16modk(d-k, 8*k + j) / (double)(8*k + j);
        
        /*
        for(int k=d+1; k&lt;=2*d; ++k)
            d16Sj += System.Math.pow(16.0d, d-k) / (double)(8*k + j);
         */
        
        return d16Sj - System.Math.Floor(d16Sj);
    }
    /// &lt;summary>
    /// Bináris hatványozás mod k, a 16^n mod k kiszámítása.
    /// &lt;/summary>
    /// &lt;param>
    /// n   kitevő
    /// &lt;/param>
    /// &lt;param>
    /// k   modulus
    /// &lt;/param>
    public static long n16modk(int n, int k) {
        
        int t = 1;
        while(t &lt;= n)
            t *= 2;
        
        long r = 1;
        
        while(true) {
            
            if(n >= t) {
                r = (16*r) % k;
                n = n - t;
            }
            
            t = t/2;
            
            if(t &lt; 1)
                break;
            
            r = (r*r) % k;
            
        }
        
        return r;
    }
    /// &lt;remark>
    /// A [BBP ALGORITMUS] David H. Bailey: The
    /// BBP Algorithm for Pi. alapján a
    /// {16^d Pi} = {4*{16^d S1} - 2*{16^d S4} - {16^d S5} - {16^d S6}}
    /// kiszámítása, a {} a törtrészt jelöli. A Pi hexa kifejtésében a
    /// d+1. hexa jegytől
    /// &lt;/remark>
     public static void Main(System.String[]args) { 
        
        double d16Pi = 0.0d;
        
        double d16S1t = 0.0d;
        double d16S4t = 0.0d;
        double d16S5t = 0.0d;
        double d16S6t = 0.0d;
        
        int jegy = 0;
        
        System.DateTime kezd = System.DateTime.Now;
        
        for(int d=1000000; d&lt;1000001; ++d) {
            
            d16Pi = 0.0d;
            
            d16S1t = d16Sj(d, 1);
            d16S4t = d16Sj(d, 4);
            d16S5t = d16Sj(d, 5);
            d16S6t = d16Sj(d, 6);
            
            d16Pi = 4.0d*d16S1t - 2.0d*d16S4t - d16S5t - d16S6t;
            
            d16Pi = d16Pi - System.Math.Floor(d16Pi);
            
            jegy = (int)System.Math.Floor(16.0d*d16Pi);
            
        }
        
        System.Console.WriteLine(jegy);
        System.TimeSpan delta = System.DateTime.Now.Subtract(kezd);
        System.Console.WriteLine(delta.TotalMilliseconds/1000.0);
    }
} </programlisting></para>
        <para>Futásról készült kép:</para>
        <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/PiBPP_cs.png"/>
                </imageobject>
            </inlinemediaobject></para>
        <para>
            <table frame="all">
                <title>A táblázat:</title>
                <tgroup cols="4">
                    <colspec colname="c1" colnum="1" colwidth="1*"/>
                    <colspec colname="c2" colnum="2" colwidth="1*"/>
                    <colspec colname="c3" colnum="3" colwidth="1*"/>
                    <colspec colname="c4" colnum="4" colwidth="1*"/>
                    <thead>
                        <row>
                            <entry>Nyelv</entry>
                            <entry>10^6</entry>
                            <entry>10^7</entry>
                            <entry>10^8</entry>
                        </row>
                    </thead>
                    <tbody>
                        <row>
                            <entry>C</entry>
                            <entry>1.414027</entry>
                            <entry>16.533379</entry>
                            <entry>193.055088</entry>
                        </row>
                        <row>
                            <entry>C++</entry>
                            <entry>1.41627</entry>
                            <entry>16.6983</entry>
                            <entry>195.65</entry>
                        </row>
                        <row>
                            <entry>C#</entry>
                            <entry>1.299032 </entry>
                            <entry>15.104768</entry>
                            <entry>174.147434</entry>
                        </row>
                        <row>
                            <entry>Java</entry>
                            <entry>1.293</entry>
                            <entry>15.153</entry>
                            <entry>171.864</entry>
                        </row>
                    </tbody>
                </tgroup>
            </table>
        </para>
        

















    </section>



   

       
 









   <section>
        <title>Ciklomatikus komplexitás</title>




	<para>
	A ciklomatikus komplexitás egy szoftvermetrika, a vezérlési gráfban megtalálható független utak maximálisszáma. Két út független, ha mindkettőben létezik olyan pont vagy él, amelyik nem eleme a másik útnak. A metrika egy adott program forráskódjának vizsgálatával határozza meg.  A ciklomatikus komplexitás arra jellemző, hogy a program vezérlés szempontjából mennyire bonyolult.
	</para>
	<para>
	A számérték kiszámításához a program utasításaival feleltetjük meg a vezérlési gráf csomópontjait, a végrehajtásuk sorrendjét, pedig a gráfban az irányított élek jelölik.  Egy döntési utasítésnak megfelelő csomópontból több él indul ki, a vezérlési ágak csomópontjába több él fut be és a ciklust önmagába visszatérő él jelöli.
	</para>
	<para>
	A vezérlési gráfunk leképezése után már csak az érték kiszámításához használt algoritmust kell használnunk, amelyet Thomas J. McCabe publikált 1976-ban, ami a következő: <emphasis role="italic"><emphasis role="bold">M</emphasis> = E − N + 2P</emphasis>, ahol M a konkrét számérték  (az  alkotója  tiszteletéből),  az  E  a  gráf  éleinek  számát,  az  N  a  csúcsok  számát,  a  P  pedig  az összefüggő komponensek számát jelölik.
	</para>
        <para>
Szerencsére, mivel az egész művelet könnyen beprogramozható, így az interneten fellelhető Lizard code complexity analyzert fogom használni, amit direkt erre a célrahoztak létre.
	</para>
        <para>A következő programot analizáltam:</para>        
        <para>
	<programlisting>
#include &lt;iostream>

int main()
{
    char choice;
    std::cout &lt;&lt;"Please choose between a or b! ";
    std::cin >>choice;
    if (choice=='a')
        std::cout &lt;&lt;"You chose a! \n";
    else if (choice=='b')
        std::cout &lt;&lt;"You chose b! \n";
    else
        std::cout &lt;&lt;"You didn't choose. :( \n";
}


	</programlisting>
	</para>	

        <para>A Lizard analizálás után kiszámolta, hogy a program ciklomatikus komplexitása 3.</para>
        <para><inlinemediaobject>
                <imageobject>
                    <imagedata fileref="img/complexity.png"/>
                </imageobject>
            </inlinemediaobject>
	</para>
        
    </section>
       
        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
