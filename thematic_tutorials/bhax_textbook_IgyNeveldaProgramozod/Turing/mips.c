#include <stdio.h>
#include <stdbool.h>

int main()
{
    printf("Kérek egy számot: ");
    int a;
    int count=0;
    scanf("%d",&a);
    while (a!=0) //ha túlcsordul, véget ér a ciklus
    {
        a<<=1;
        count++;
    }
    printf("%d\n",count);
}