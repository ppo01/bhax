<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Berners-Lee!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>



 <section>
        <title>C++ és JAVA összehasonlítása</title>
        <para>437-465 = 28 oldal</para>
        <para>Benedek Zoltán, Levendovszky Tihamér Szoftverfejlesztés C++ nyelven és Nyékyné Dr.
            Gaizler Judit et al. Java 2 útikalauz programozók című könyve alapján először is
            hasonlítsuk össze, mik is a közös vonások és mik az eltérések a két nyelv között. </para>
        <para>A szintaxis a két nyelv között nagyon hasonló, ugyanis anno a Java nyelv fejlesztői a
            C és C++ alapvető szitanxisát vették figyelembe. Ezért lehetséges az, hogy bizonyos
            kódcsipetek mind a két nyelvben lefordulnak, és sokszor még hasonló jelentéssel is
            bírnak. Ez azonban nem minden esetben igaz, és egy egész forráskód nagy valószínűséggel
            nem lesz helyes mind a két nyelvben, mégha a szintaxis hasonló is. </para>
        <para>A Java nyelv szűkebb, mint a C++, de a szabványos osztálykönyvtárai szélesebb
            alkalmazási területeket fed le, például nyelvi szinten támogatja a végrehajtási széleket
            (angolul <emphasis>thread</emphasis>), grafikus felhasználói felület programozását vagy
            például a hálózati programozást is. Ezekre azért a C++ egyedül nem képes, egy egyedi,
            külső könyvtárra van szüksége, hogy képes legyen ezeket kezelni. </para>
        <para>A C++ lehetőséget nyújt arra, hogy forrásszinten hordozható legyen a program, mert az
            adott gép szabványos módon újrafordítja és egy helyesen futó programot ad eredményül.
            Azonban a már lefordított, bináris kód nem hordozható, csak speciális esetekben, hiszen
            a lefordított kód tartalmaz olyan feltételezéseket, amik az adott operációs rendszerre
            vonatkoznak. Ezzel szemben a Java platform független, lehetőséget ad a bináris kód
            hordozására, mivel ahogy átvisszük, egy szabványos Java virtuális gép környzetetet
            látunk. Sőt, lehetséges az is, hogy már a futási időben átkerül egy másik gépre. Ahhoz,
            hogy a hordozható kód viselkedése kevésbé függjön a platformtól és az implementációtól,
            a Java kevesebb dolgot bíz az impelemtációra, ennek nyomán pedig mondjuk a kiértékelési
            sorrend vagy a mellékhatások részletei pontosabban definiálva vannak, mint a C és
            C++-ban. Továbbá a Java olyanokat is ellenőriz, amit az előző két nyelv nem, vagy
            maximum csak figyelmeztetést ad rá, ilyen például mikor egy lokális változónak nem
            adtunk értéket. Például a Java hibát dob ki egy ilyen esetben: </para>
        <para>
            <programlisting>public class y {
    public static void main(String argv[]) {
        int i;
        System.out.println(i);
    }
}</programlisting>
        </para>
        <para>Hiszen itt nem adtunk meg értéket az <code>i</code>-nek, tehát nem is tud igazából
            semmit kiírni a program. De ha már ezt beraknánk a deklaráció után, akkor már hiba
            üzenet kiírása nélkül, helyesen lefutna a kód, elvégre az if utasítás mindegyik ágában
            kap egy értéket az <code>i</code>: </para>
        <para>
            <programlisting>if(argv.length == 0) 
    i = 1;
else 
    i = 2</programlisting>
        </para>
        <para>Akkor is hibát jelezne ki a program, ha az if utasítások nem garantálnának értéket az
                <code>i</code> változónak, ezért biztosra megy. Tehát nagyon fontos a Java-ban, hogy
            a változóknak legyen egy értéke valahol, amit biztosan megkap, különben hiába ütközünk.
            Másik problémaforrás lehet az, mikor olyan része is van a kódnak, ami elérhetetlen, mert
            mondjuk előbb adjuk ki a programnak, hogy fejezze be a futást, mint hogy kiíratnánk
            valamit. Íme egy példa: </para>
        <para>
            <programlisting>void doit(int i) {
    return;
    System.out.println(i);    //már nem jut el ide a művelet
}</programlisting>
        </para>
        <para>Hibaüzenettel találkozunk abban az esetben is, mikor egy nem void metódus (vagyis
            függvény) valóban visszaad-e egy értéket. Ez a fajta szigorú ellenőrzés azoknak, akik a
            C++-ot szokták meg, és abban szeretnek programozni, bosszantó lehet, noha egyébként
            megvan ezeknek az ellenőrzéseknek a hasznossága. Például átok a Courier New
            betűtípusnak, amit programozás során szoktunk használni, hogy az L betű és az 1-es
            hasonlóan néz ki. Így történhet meg az, hogy default helyett defau1t-ot írunk, és nem
            tűnik fel. A C++ ezen simán elsiklik, így magunknak kell megkeresnünk a problémát - bár
            ma már vannak szintaxis-érzékeny szerkesztők -, de a Java ezekre különösen érzékeny és
            azonnal hibajelzést ad ki, ami nagyon hasznos. </para>
        <para>Nagy különbségek vannak a két nyelv objektummodellje között, amikkel feltétlenül
            tisztában kell lennünk. A C++ esetén ugyebár az objektumokat a memória egy összefüggő
            területén elhelyezkedő bájtsorozatként fogja fel, ami memóriakiosztással rendelkezik, és
            amit ennek megfelelően manipulál a legfordított program. Mutatók által még manipulálni
            is tudjuk a memóriát akár. Háromféle módon tudjuk használni a memóriát: van statikus
            memória, automatikus memória és végül a szabad (dinamikus) tár, amit a programozó kezel. </para>
        <para>Ezzel ellentétben a Java nem éri el közvetlenül a memóriát, hanem csak hivatkozásokon
            keresztül, tehát virtuális gépben fut a program. Az osztályok önálló class-fájllá
            fordunal le, amik szabványosak és platform-függetlenek, és a Java virtuális gép pontosan
            ezeket ellenőrzi betöltéskor. A C++-szal ellentétben a Java ismeri is az osztály
            kompatibilis megváltoztatásának fogalmát. Ez annyit jelent, hogyha például a hivatkozott
            osztályban megváltozik a változók deklarációs sorrendje vagy új tagot hozunk létre,
            akkor a hivatkozó osztály újrafordítás nélkül is érvényes marad. Megkülönböztet továbbá
            primitív típusokat és objektumokat, a kettő között az a különbség, hogy az utóbbiak
            hivatkozásokon keresztül érünk el. Ezeket a Java virtuális gép egy automatikus
            szemétgyűjtő mechanizmus által felügyelt véletlen elérésű tárterületen tárolja, mivel
            nem lehetnek statikus adatterületen. A Java-ban nincsen programozói tár felszabadítás,
            hiányzik a destruktor-mechanizmus. </para>
        <para>A C++ nyelvben az objektumok élettartamát pontosan lehet tudni, hiszen automatikus
            változók esetében ezek a blokk belépésének idejében keletkeznek, és megszűnnek, mikor a
            blokk végrehajtása befejeződik. A statikus élettartamú objektumok egészen a program
            végéig élnek, és vannak olyanok, akiknek mi mondjuk meg az élettartamát. A Java-ban
            viszont nincsen érvénytelen hivatkozás, vagyis amíg van referenciánk egy objektumra, az
            addig él. </para>
        <para>A szoftverfejlesztés két fontos fogalma az absztrakció és a paradigma. Az előbbi azt
            jelenti, hogy kiemelünk egy rendszer részeinek közös tulajdonságait és elvonatkoztatunk
            bizonyosak eltérésétől. Az utóbbi fogalom pedig szokásjogot jelent, vagyis ez mondja
            meg, hogyan alkalmazzik az absztrakciót. A C++ többparadigmás, ami annyit jelent, hogy
            írhatunk procedurális programokat, egymást hívó függvényekkel, változókkal.
            Alkalmazhatjuk az objektumorientáltságot, létrehozhatunk generikus elvő programokat,
            könyvtárakat, sőt ezeket még vegyíthetjük is. Ezzel ellentétben a Java csak az
            objektumorientált-elveket vallja, ezt jelzi az, hogy nincsenek globális változók,
            függvények, mindegyik egy-egy osztályhoz tartozik (hiszen maga a
                <function>main</function> is egy osztály publikus metódusa). </para>
    </section>














































<section>
	<title>Programozás Python nyelven</title>
	<para>
Ez a könyv a mobilprogramozás sajátosságaiba ad betekintést, azonban én most csak a Python nyelvre vonatkozó bevezető részről fogok beszámolni. A Python egy objektumorientált, magas szintű programozási nyelv, hasonlóan például a C, vagy a C++ nyelvekhez. A Python továbbá egy szkriptnyelv, ami azt jelenti, hogy a kódunkat, magát a programot az értelmező a futtatás során értelmezi szemben más nyelvekkel, ahol fordítóprogramra van szükségünk, hogy futtatható állományt készítsünk a kódunkból. Ez nagyon sok esetben megkönnyíti a dolgunkat, hiszen nem kell minden egyes apró javítás esetén újrafordítanunk a kódot, hiszen a gép megteszi maga. A nyelv leginkább prototípus-programok tesztelésére, illetve egyszerűbb alkalmazások elkészítésére alkalmas.
	</para>
	<para>
Számos lényeges eltérés van más nyelvek, mint a C++ és a Python között. A kódban nincs szükség elválasztójelekre (pontosvessző, kapcsos zárójelek, stb..), helyette tabulátorozással, tördeléssel oldja meg a problémát. Továbbá a változóknak és függvényargumentumoknak nem kell megadni előre a típusát (pl. int, string, stb..), hiszen az értelmező automatikusan felismeri azt futtatáskor. Valamint egy másik szembetűnő különbség, hogy míg C-ben sokkal nagyobb munkával, kódolással tudjuk elérni a célunkat, addig Pythonban bonyolultabb kifejezéseket tudunk leírni rövidebben, tömörebben.
	</para>
	<para>
Ahogy az előbb említettem, Pythonban tabulátorokkal, illetve spacekkel tagoljuk a kódunkat. Az azonos behúzású sorok egy logikai egységet alkotnak.
	</para>
	<para>
A kódban minden sort úgynevezett tokenekre bont az értelmező, ezek a tokenek a különféle megadható karakterek, szavak, stb... amelyeket felhasználhatunk. Különböző típusai: azonosítók (változók, függvények nevei, stb..), kulcsszavak (pl. if, else, elif, and, or, stb..), delimiterek/elválasztójelek (pl. vessző, kettőspont), literálok (állandók/konstansok) és az operátorok, mint például az összeadás- vagy kivonásjel ("+","-").
	</para>
	<para>
A C-hez és C++-hoz hasonlóan a Python is objektumorientált nyelv, és az objektumoknak/adatoknak különféle típusaik lehetnek, ezek a következők: számok (egész, illetve lebegőpontos és komplex számok), sztringek (az 'u' előtag hozzátevésével Unicode kódolású sztringeket adhatunk meg), ezeket idézőjelek közé kell tennünk. Felvehetünk adat n-eseket is (angolul tuples), illetve listákat is, a különbség a kettő között csupán annyi, hogy az utóbbi mérete módosítható, azaz tudunk felvenni a listába további adatokat. Ezen kívül megadhatunk szótárakat is, ez lényegében adatpárokból épül fel, minden eleméhez tartozik egy azonosító vagy kulcs, amely egyértelműen meghatározza azt.
	</para>
	<para>
Ahogy korábban említettem már, a változóknak nincsen típusa a kódban Pythonban, így a felhasználásuktól függően különböző típusú adatokra is vonatkozhatnak futtatás során akár. Egy sorban egyszerre több változónak is adhatunk értéket (azonos értéket), létrehozhazunk sorozatokat is, valamint akár 2 változó értékét is felcserélhetjük az a,b=b,a egyszerű kifejezéssel, ezzel szemben C nyelven például azt nem tudjuk ilyen könnyen megtenni. Változókat a global kulcsszóval tehetünk globálissá.
	</para>
	<para>
A különböző típusú számok, pl. int-float között létezik konverzió. Sztringeken, listákon, szekvenciákon különböző műveleteket végezhetünk, összefűzhetjük őket (a + operátorral), lekérhetjük a hosszukat (a len függvénnyel), stb. Egy szekvent adott indexű elemét szögletes zárójellel érjuk el, pl. a[2] az a szekvent 3. eleme, láthatjuk, hogy az indexelés itt is 0-tól indul. Listákon végezhetünk különféle műveleteket függvények segítségével, például lekérhetjük egy adott indexű elemét, sorbarendezhetjük, kitörölhetünk vagy hozzáadhatunk elemet, stb. Hasonlóan szótárakon is értelmezve vannak különféle függvények, például elemeket törölhetünk, megkereshetünk benne egy adott kulcsot, lekérhetjük a különböző adatokat, kulcsokat, adat-kulcspárokat.
	</para>
	<para>
A nyelv egyik legalapvetőbb függvénye a print() függvény, amely értelemszerűen a standard outputra, konzolra ír ki.
	</para>
	<para>
A többi nyelven hasonló módon működnek az elágazások, azaz az if, elif (vagyis az else if) és az else kifejezések, amikről nem is nagyon kell beszélni sokat.
	</para>
	<para>
A ciklusok, azaz a while, és a for ciklus is hasonlóan működnek, kis kiegészítéssel. For esetében használhatjuk az in kulcsszót, és a range() függvényt, amely megmondja, hogy a ciklusváltozó milyen intervallumban mozogjon. A gyakorlatban: pl. for i in range(0,10). A while esetében a feltétel körül elhagyható a zárójel, és a végére kettőspontot kell tennünk.
	</para>
	<para>
Egyedi megoldás a címkék (labelek) használata. A goto 'címke' kulcsszó megadásával a kód futásakor az értelmező a címke sorához ugrik, és onnan folytatódik a végrehajtás. A comefrom 'címke' kifejezéssel pedig az ellenkezőjét érjük el: az előtte lévő címkéről a comefrom sorára ugrik, majd innen folytatódik tovább.
	</para>
	<para>
A függvények esetében a megszokott dolgokon kívül csak néhány változtatás van: például csak érték szerint adhatunk át paramétert, kivéve pl. lista, szótár esetében, ott a hívott függvény megváltoztathatja a hívó függvény által átadott paramétereket. A def kulcsszóval definiáljuk őket.
	</para>
	<para>
Az osztályok (classok) esetében is hasonló a helyzet, mint sok más nyelvnél: definiálunk egy osztályt, amelynek lehetnek tagfüggvényei, illetve tagobjektumai (változók). A függvényeinek kötelezően meg kell adni egy 'self' nevű első paramétert, ez azt az objektumot adja meg, amellyel meghívták a függvényt.
	</para>
	<para>
A C++-ból is ismert try-catch-es kivételkezeléshez hasonlóan a Python is támogatja a kivételkezelést. A try kulcsszó után áll a vizsgálandó kódrészünk, amely lefutásában keressük a hibát. A try blokk után áll az except blokk: ez a rész akkor fut le, amikor a try blokkban előáll a kivételes esetünk. További, nem kötelező rész az ezután álló else ág.
	</para>
	<para>
Lehetőség van Pythonban továbbá modulok készítésére, felhasználására, importálására. A modulok olyan kódrészletek, fájlok, amelyek felhasználó által készített függvényeket, eljárásokat, konstansokat, stb. tartalmaznak, és ezek felhasználhatók más Python programokban. Egy programban az import szóval tudunk modulokat importálni, pl.: from 'modulnév' import 'függvény'.
	</para>
    </section>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
</chapter>                
